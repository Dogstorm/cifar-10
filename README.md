# CIFAR 10

## Image classification on the famous CIFAR10 dataset.

### Tests

This repository contains 2 notebooks, in the notebook "tests" I tested different types of simple networks on the CIFAR 10 dataset.

I also tried transfer learning with the ResNet50 model.

Here are the learning curves of the accuracy of respectively a handmade CNN and ResNet50 :

<div align="center">
<img src="images/accuracyCNN.png">
<img src="images/accuracyResNet.png">
</div>

**The results were satisfying with an accuracy of 80% to 84% on the validation set but they could be improved.**

### A more effective network

The notebook "serious attempt" presents a deep and flexible network, using several types of regularizations and techniques to speed up convergence.

The interesting features of this network are :

-The **batch normalization** before the convolutional layers

-The regularization with **Dropout** layers and **L2 regularization** on the convolutional layers

-The **exponential linear unit** activation function. This allows a faster convergence and better accuracy, this article gives a detailed explaination of this activation function : https://arxiv.org/abs/1511.07289

-The changing learning rate in the Adam optimizer, as shown in the learning curves changing it once a certain accuracy is reached allows a more effective training.

This is the learning curve of the accuracy with this network :

<div align="center">
<img src="images/accuracy.png">
</div>

The improvement at around 80 epochs corresponds to the modification of the learning rate, a lower learning rate allowed the network to train more precisely.

**Implementing all those features in a deep network with 6 convolutional layers allowed the model to reach an accuracy of 89% on the validation data.**


### Code

The details of the networks and the conditions of the training are available in the notebooks with some details and comments about the training and the results.